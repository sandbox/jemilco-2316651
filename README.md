# Most Popular RSS #

Creates RSS feeds from each of the Most Popular services and interval.

## URL format ##

### Drupal 6: 
/mostpopular/items/%service/%interval/rss.xml

### Drupal 7: 
/mostpopular/%block/%service/%interval/rss.xml